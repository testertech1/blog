---
layout: post
title:  "Eww from scratch in Hyprland (Fedora 38)"
date:    2023-10-27 
categories: eww
---

# Introduction
There are some good guides already. Highly recommend the [guide](https://elkowar.github.io/eww/) in the Eww documentation. However, on this page I'm going to make it concise to Fedora 38. 

# Hyprland
## Copr
Getting Hyprland on Fedora which is not a standard package atm. Therefor, 
check the Copr [solophasa/hyprland](https://copr.fedorainfracloud.org/coprs/solopasha/hyprland/) 
there are multiple Copr's repo's that contain a Hyprland package. In the video which this page 
refers I use the on from solophasa. Just add it ```sudo dnf copr enable solopasha/hyprland``` 
and install hyprland: ```sudo dnf install hyprland```. After logging out you should see the 
hyprland session as an option in your login screen. Kitty is really required when you first 
start Hyprland. It's defined as a recommended package so that will be installed. 

### Hyprpaper
I've also played around with Hyprpaper recently and it work fine although a little basic and doesn't support dynamic wallpaper loading. 

### Ags
And at the time of writing this I just saw that Aylurs Layer Shell is included in this copr. 

I have a simple config you can use but ofcourse you can use your own config. 

# Eww required packages 
- gtk-layer-shell
- glib2

## Rust
Check this nice onliner to get Rust [rustup](https://rustup.rs/) 
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
After that you should have cargo, so you can run the cargo build command later. 

## source code
```git clone https://github.com/elkowar/eww```

## build 
```cargo build --release --no-default-features --features=wayland```

# Eww config

## Hello World! 
- https://github.com/TesterTech/eww-from-scratch/tree/main/dot_config/eww/ewwbarfromscratch

## More extensive example 
- https://github.com/TesterTech/eww-from-scratch/tree/main/dot_config/eww/ewwbarhyprland